export const APPS = [
  {
    id: 1,
    name: 'App 1',
    update: false,
  },
  {
    id: 2,
    name: 'App 2',
    update: false,
  },
  {
    id: 3,
    name: 'App 3',
    update: false,
  },
  {
    id: 4,
    name: 'App 4',
    update: false,
  },
  {
    id: 5,
    name: 'App 5',
    update: false,
  },
];

export const NOTHING = '';
