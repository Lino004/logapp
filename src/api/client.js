import HTTP from './HTTP';

const URL_BASE = 'clients';

export async function GetClients() {
  const response = await HTTP.get(`${URL_BASE}`);
  return response;
}

export async function CreateClient(data) {
  const response = await HTTP.post(`${URL_BASE}`, data);
  return response;
}

export async function GetLogsClient(id) {
  const response = await HTTP.get(`${URL_BASE}/${id}/logs`);
  return response;
}
