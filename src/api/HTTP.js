import axios from 'axios';
import config from './config';
import store from '../store';
// import router from '../router';

const HTTP = axios.create();

HTTP.interceptors.request.use((request) => {
  // eslint-disable-next-line no-param-reassign
  request.headers = {
    'Content-Type': 'application/json',
    Authorization: `Bearer ${store.getters.token}`,
  };
  if (request.url.indexOf('http') === -1) {
    // eslint-disable-next-line no-param-reassign
    request.url = config.BASE_URL + request.url;
  }
  return request;
}, (error) => Promise.reject(error));

HTTP.interceptors.response.use(
  (response) => response,
  (error) => {
    if (error !== undefined && error.response !== undefined && error.response.status === 401) {
      store.dispatch('user/logout');
      // router.go('/connexion');
      return;
    }
    throw error;
  },
);

export default HTTP;
