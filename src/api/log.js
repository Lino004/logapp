import HTTP from './HTTP';

const URL_BASE = 'logs';

export async function GetAll() {
  const response = await HTTP.get(`${URL_BASE}`);
  return response;
}

export async function Create(data) {
  const response = await HTTP.post(`${URL_BASE}`, data);
  return response;
}
