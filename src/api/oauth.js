import qs from 'qs';
import axios from 'axios';
import configApi from './config';

const URL_BASE = 'oauth';

export async function GetToken(data) {
  const req = qs.stringify(data);
  const clientid = 'sankara-front';
  const clientSecret = '5615ff92-3265-49fe-a1f7-2fd17a4add05';
  const token = `${clientid}:${clientSecret}`;
  const encodedToken = Buffer.from(token).toString('base64');
  const config = {
    method: 'post',
    url: `${configApi.BASE_URL + URL_BASE}/token`,
    headers: {
      Authorization: `Basic ${encodedToken}`,
      'Content-Type': 'application/x-www-form-urlencoded',
    },
    data: req,
  };
  const response = await axios(config);
  return response;
}

export const NOTHING = '';
