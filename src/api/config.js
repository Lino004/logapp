const configProd = {
  BASE_URL_API: 'http://167.86.100.164:4002',
  BASE_URL: 'http://167.86.100.164:4002/api/',
};
const configDev = {
  BASE_URL_API: 'http://167.86.100.164:4002',
  BASE_URL: 'http://167.86.100.164:4002/api/',
};

// eslint-disable-next-line import/no-mutable-exports
let config = configDev;

if (process.env.NODE_ENV === 'production') {
  config = configProd;
}

export default config;
