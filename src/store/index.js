import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    security: null,
  },
  mutations: {
    SET_DATA_AUTH: (state, data) => {
      state.security = data;
    },
    RESET: (state) => {
      state.security = null;
    },
  },
  actions: {
  },
  getters: {
    isConnected: (state) => !!state.security,
    token: (state) => state.security.accessToken || null,
  },
  plugins: [
    createPersistedState({
      key: 'app_log',
      paths: ['security'],
    }),
  ],
});
