export function toFormData(data) {
  const formdata = new FormData();
  Object.keys(data).forEach((key) => {
    formdata.append(key, data[key]);
  });
  return formdata;
}

export const NOTHING = '';
