import Vue from 'vue';
import Buefy from 'buefy';
import App from './App.vue';
import router from './router';
import store from './store';
import './assets/scss/app.scss';
import './registerServiceWorker';

Vue.use(Buefy);

Vue.config.productionTip = false;

Vue.mixin({
  methods: {
    errorToast(error) {
      let { message } = error;
      if (error.response && error.response.data) {
        message = error.response.data.message;
      }
      this.$buefy.toast.open({
        duration: 5000,
        message,
        position: 'is-bottom-right',
        type: 'is-danger',
      });
    },
  },
});

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
