import Vue from 'vue';
import VueRouter from 'vue-router';
import store from '../store';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    component: () => import('../views/app/Index.vue'),
    meta: {
      requiresAuth: true,
    },
    children: [
      {
        path: '',
        name: 'Home',
        component: () => import('../views/app/Home.vue'),
      },
      {
        path: '/log-app/:id',
        name: 'LogAppById',
        component: () => import('../views/app/LogApp.vue'),
      },
    ],
  },
  {
    path: '/connexion',
    name: 'Connexion',
    component: () => import('../views/Connexion.vue'),
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to, from, next) => {
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    if (!store.getters.isConnected) {
      next({ name: 'Connexion' });
    } else {
      next();
    }
  } else {
    next();
  }
});

export default router;
